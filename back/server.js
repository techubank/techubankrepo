var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var cors = require('cors');
var app = express();

//variable para poder usar el request-json
var requestJson = require('request-json');
//postgreSQL
var pg= require('pg');
//"postgres://localhost:5433/bdSeguridad";
var urlUsuarios = "postgres://docker:docker@postgresql:5432/bdseguridad";
//asumo recibo req.body = {usuario:xxxxx, pasword:xxxxx}
var clientePostgre = new pg.Client(urlUsuarios);


//variable para que se pueda parsear el req.body y no salga como undefined
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // for parsing application/json
app.use(cors());

var path = require('path');

// Conexión Mlab
var urlMlab = "https://api.mlab.com/api/1/databases/bdbanca/collections/ctasmov";
var apiKey = "apiKey=S26mnsf3dKrF5mRlymz9h6qAF3jkABen";

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.post('/login', function(req, res) {
  clientePostgre.connect();
  //console.log("conectado a base de datos postgreSQL");
  //console.log("dni: " + req.body.dni + " password: " + req.body.password);
  const query = clientePostgre.query('SELECT idusuario, nombre FROM usuarios WHERE dni =$1 AND password=$2;'
  , [req.body.dni, req.body.password]
  , (err, result) => {
    if(err){
      console.log(err);
      res.send(err);
    }
    else{
      //console.log("rowcount es :" + result.rows.length);
      if(result.rows.length==1) {
        //console.log("Login correcto idusuario: " + result.rows[0].idusuario);
        //console.log("Login correcto nombre: " + result.rows[0].nombre);
        var jsonLogin={};
        jsonLogin.idusuario = result.rows[0].idusuario.toString();
        jsonLogin.nombre = result.rows[0].nombre;
        var respuestaLogin = JSON.stringify(jsonLogin);
        //console.log(respuestaLogin);
        res.send(respuestaLogin);
      }
      else{
        //console.log("Login incorrecto");
        var jsonLogin={};
        jsonLogin.idusuario = "-1";
        jsonLogin.nombre = "";
        var respuestaLogin = JSON.stringify(jsonLogin);
        //console.log(respuestaLogin);
        res.send(respuestaLogin);
      }
    }
  });
});

app.post('/register', function(req, res) {
  clientePostgre.connect();
  //console.log("Registro de usuarios");
  //console.log("conectado a base de datos postgreSQL");
  //console.log("nombre: " + req.body.nombre + " apellidos: " + req.body.apellidos
  //+ " direccion: " + req.body.direccion + " provincia: " + req.body.provincia
  //+ " codPostal: " + req.body.codPostal + " email: " + req.body.email
  //+ " dni: " + req.body.dni + " password: " + req.body.password);

  const query = clientePostgre.query('INSERT INTO usuarios (dni, password, nombre, apellidos, direccion, cp, provincia, email) VALUES ($1, $2, $3, $4, $5, $6, $7, $8);'
    , [req.body.dni, req.body.password, req.body.nombre, req.body.apellidos
    , req.body.direccion, req.body.codPostal, req.body.provincia, req.body.email]
    , (err, result) => {
      if(err)
      {
        console.log(err);
        //console.log("Voy a construir el jsonRegister del insert");
        var jsonRegister={};
        jsonRegister.idusuario = "-1";
        jsonRegister.nombre = "";
        var respuestaRegister = JSON.stringify(jsonRegister);
        //console.log(respuestaRegister);
        res.send(respuestaRegister);
      }
      else
      {
      //console.log("Valido que se ha hecho el registro y recupero el id");
      //console.log("dni: " + req.body.dni + " password: " + req.body.password);

      const query = clientePostgre.query('SELECT idusuario, nombre FROM usuarios WHERE dni =$1 AND password=$2;'
          , [req.body.dni, req.body.password]
          , (err, result) => {
            if(err)
            {
              console.log(err);
              //console.log("Voy a construir el jsonRegister del select");
              var jsonRegister={};
              jsonRegister.idusuario = "-1";
              jsonRegister.nombre = "";
              var respuestaRegister = JSON.stringify(jsonRegister);
              //console.log(respuestaRegister);
              res.send(respuestaRegister);
            }
            else
            {
              //console.log("rowcount es :" + result.rows.length);
              if(result.rows.length==1)
              {
                //console.log("Usurio registrado idusuario: " + result.rows[0].idusuario);
                //console.log("Usuario registrado nombre: " + result.rows[0].nombre);
                var jsonRegister={};
                jsonRegister.idusuario = result.rows[0].idusuario.toString();
                jsonRegister.nombre = result.rows[0].nombre;
                var respuestaRegister = JSON.stringify(jsonRegister);
                //console.log(respuestaRegister);
                res.send(respuestaRegister);
              }
              else
              {
                //console.log("Registro Incorrecto");
                var jsonRegister={};
                jsonRegister.idusuario = "-1";
                jsonRegister.nombre = "";
                var respuestaRegister = JSON.stringify(jsonRegister);
                //console.log(respuestaRegister);
                res.send(respuestaRegister);
              }
            }
          });
        }
    });
});

app.get('/cuentas/:idusuario', function(req, res) {
    //console.log("Entro por /cuentas/:idusuario");
    //console.log(req.params.idusuario);
    //console.log(urlMlab
    //  + "?q={'idusuario':" + req.params.idusuario
    //  + "}&f={_id:0,'idusuario':0,'cuentas.movimientos':0}&" + apiKey);
    clienteMlab = requestJson.createClient(urlMlab
      + "?q={'idusuario':" + req.params.idusuario
      + "}&f={_id:0,'idusuario':0,'cuentas.movimientos':0}&" + apiKey);
    clienteMlab.get('', function(err, resM, body) {
      if(err)
      {
        console.log(resM);
      }
      else
      {
        //Ya tenemos en body la informacion de las cuentas del usuario
        //Ahora vamos a editar los saldos para mostrarlos por ventana
        var importenumero = 0;
        var importecadena = ";"
        var importeeditado = "";
        var signo = "";
        var ix = "";
        var j = 0;

        for( var i = 0; i < body[0].cuentas.length; i++ ) {
          importenumero = parseFloat(body[0].cuentas[i].saldo);
          importecadena = body[0].cuentas[i].saldo;
          signo = importenumero < 0 ? "-" : "";
          ix = String(parseInt(importenumero = Math.abs(Number(importenumero) || 0).toFixed(2)));
          j = (j = ix.length) > 3 ? j % 3 : 0;
          importeeditado = signo + (j ? ix.substr(0, j) + "." : "") + ix.substr(j).replace(/(\","{3})(?=\",")/g, "$1" + ".") + (2 ? "," + Math.abs(importenumero - ix).toFixed(2).slice(2) : "");
          //console.log("importe editado " + i + " " + importeeditado);
          body[0].cuentas[i].saldo = importeeditado;
          importecadena = "";
          importeeditado = "";
          signo = "";
          ix = "";
          j = 0;
        };

        var respuestaBody = JSON.stringify(body);
        //console.log(respuestaBody);
        res.send(respuestaBody);
      }
    });
});

app.get('/movimientos/:idusuario/:idcuenta', function(req, res) {
    //console.log("Entro por /movimientos/:idusuario/:idcuenta");
    //console.log(req.params.idusuario);
    //console.log(req.params.idcuenta);
    //console.log(urlMlab
    //  + "?q={'idusuario':" + req.params.idusuario
    //  + ",'cuentas.idcuenta':'" + req.params.idusuario + "-" + req.params.idcuenta
    //  + "'}&f={_id:0,'cuentas':0,'cuentas.movimientos':1,'cuentas':{'$elemMatch':{'idcuenta':'"
    //  + req.params.idusuario + "-" + req.params.idcuenta + "'}}}&" + apiKey);
    clienteMlab = requestJson.createClient(urlMlab
      + "?q={'idusuario':" + req.params.idusuario
      + ",'cuentas.idcuenta':'" + req.params.idusuario + "-" + req.params.idcuenta
      + "'}&f={_id:0,'cuentas':0,'cuentas.movimientos':1,'cuentas':{'$elemMatch':{'idcuenta':'"
      + req.params.idusuario + "-" + req.params.idcuenta + "'}}}&" + apiKey);
    clienteMlab.get('', function(err, resM, body) {
      if(err)
      {
        console.log(resM);
      }
      else
      {
        //Ya tenemos en body la informacion de los movimientos de cuenta
        //Ahora vamos a editar el importe y los saldos para mostrar por ventana
        var importenumero = 0;
        var importecadena = ";"
        var importeeditado = "";
        var signo = "";
        var ix = "";
        var j = 0;

        for( var i = 0; i < body[0].cuentas[0].movimientos.length; i++ ) {
          importenumero = parseFloat(body[0].cuentas[0].movimientos[i].importe);
          importecadena = body[0].cuentas[0].movimientos[i].importe;
          signo = importenumero < 0 ? "-" : "";
          ix = String(parseInt(importenumero = Math.abs(Number(importenumero) || 0).toFixed(2)));
          j = (j = ix.length) > 3 ? j % 3 : 0;
          importeeditado = signo + (j ? ix.substr(0, j) + "." : "") + ix.substr(j).replace(/(\","{3})(?=\",")/g, "$1" + ".") + (2 ? "," + Math.abs(importenumero - ix).toFixed(2).slice(2) : "") + " EUR";
          //console.log("importe editado " + i + " " + importeeditado);
          body[0].cuentas[0].movimientos[i].importe = importeeditado;
          importecadena = "";
          importeeditado = "";
          signo = "";
          ix = "";
          j = 0;

          importenumero = parseFloat(body[0].cuentas[0].movimientos[i].saldomovimiento);
          importecadena = body[0].cuentas[0].movimientos[i].saldomovimiento;
          signo = importenumero < 0 ? "-" : "";
          ix = String(parseInt(importenumero = Math.abs(Number(importenumero) || 0).toFixed(2)));
          j = (j = ix.length) > 3 ? j % 3 : 0;
          importeeditado = signo + (j ? ix.substr(0, j) + "." : "") + ix.substr(j).replace(/(\","{3})(?=\",")/g, "$1" + ".") + (2 ? "," + Math.abs(importenumero - ix).toFixed(2).slice(2) : "") + " EUR";
          //console.log("saldomovimiento editado " + i + " " + importeeditado);
          body[0].cuentas[0].movimientos[i].saldomovimiento = importeeditado;
          importecadena = "";
          importeeditado = "";
          signo = "";
          ix = "";
          j = 0;
        };

        var respuestaBody = JSON.stringify(body);
        //console.log(respuestaBody);
        res.send(respuestaBody);
      }
    });
});

app.get('/ingresosgastos/:idusuario/:idcuenta', function(req, res) {
    //console.log("Entro por /igresosgastos/:idusuario/:idcuenta");
    //console.log(req.params.idusuario);
    //console.log(req.params.idcuenta);
    //console.log("Agregamos importes por Tipo (Ingreos y Gastos)");
    //console.log(urlMlab
    //  + "?q={'idusuario':" + req.params.idusuario
    //  + ",'cuentas.idcuenta':'" + req.params.idusuario + "-" + req.params.idcuenta
    //  + "'}&f={_id:0,'cuentas':0,'cuentas.movimientos':1,'cuentas':{'$elemMatch':{'idcuenta':'"
    //  + req.params.idusuario + "-" + req.params.idcuenta + "'}}}&" + apiKey);
    clienteMlab = requestJson.createClient(urlMlab
      + "?q={'idusuario':" + req.params.idusuario
      + ",'cuentas.idcuenta':'" + req.params.idusuario + "-" + req.params.idcuenta
      + "'}&f={_id:0,'cuentas':0,'cuentas.movimientos':1,'cuentas':{'$elemMatch':{'idcuenta':'"
      + req.params.idusuario + "-" + req.params.idcuenta + "'}}}&" + apiKey);
    clienteMlab.get('', function(err, resM, body) {
      if(err)
      {
        console.log(resM);
      }
      else
      {
        //Sumamos todos los ingresos
        if (body[0].cuentas[0].movimientos.length != undefined) {
          var ingresos = 0;
          var gastos = 0;
          for( var i = 0; i < body[0].cuentas[0].movimientos.length; i++ ) {
            if (body[0].cuentas[0].movimientos[i].tipo == "Ingreso") {
                ingresos = ingresos + parseFloat(body[0].cuentas[0].movimientos[i].importe);
            }
            else if (body[0].cuentas[0].movimientos[i].tipo == "Gasto")
            {
                gastos = gastos + (parseFloat(body[0].cuentas[0].movimientos[i].importe) * -1);
            }
          };
          var respuestaBody = '[["Ingresos",' + ingresos + '],["Gastos",' + gastos + ']]';
          //console.log(respuestaBody);
          res.send(respuestaBody);
        }
        else
        {
          var respuestaBody = '[["Ingresos",0.00],["Gastos",0.00]]';
          //console.log(respuestaBody);
          res.send(respuestaBody);
        }
      }
    });
});

app.post('/crearcuentas', function(req, res){
  //console.log("Vamos a crear las cuentas del nuevo usuario " + req.body.idusuario);

  var fecha = new Date().toJSON().slice(0,10);

  //console.log(fecha);

  var fechaeditada = "";
  fechaeditada = fecha.substring(8,10) + "/"
               + fecha.substring(5,7) + "/"
               + fecha.substring(0,4);

  //console.log(fechaeditada);

  var nuevasCuentas =
    '{' +
       '"idusuario":' + req.body.idusuario + ',' +
       '"cuentas":[' +
         '{' +
            '"idcuenta":"' + req.body.idusuario + '-1",' +
            '"saldo":0.00,' +
            '"divisa":"EUR",' +
            '"descripcion":"Cuenta Corriente",' +
            '"movimientos":[' +
               '{' +
                   '"fecha":"' + fechaeditada + '",' +
                   '"importe":0.00,' +
                   '"tipo":"Apertura cuenta",' +
                   '"descripcion":"Apertura cuenta",' +
                   '"saldomovimiento":0.00' +
               '}' +
            ']' +
         '},' +
         '{' +
            '"idcuenta":"' + req.body.idusuario + '-2",' +
            '"saldo":0.00,' +
            '"divisa":"EUR",' +
            '"descripcion":"Cuenta Ahorro",' +
            '"movimientos":[' +
               '{' +
                   '"fecha":"' + fechaeditada + '",' +
                   '"importe":0.00,' +
                   '"tipo":"Apertura cuenta",' +
                   '"descripcion":"Apertura cuenta",' +
                   '"saldomovimiento":0.00' +
               '}' +
            ']' +
         '}' +
       ']' +
    '}'

  //console.log(nuevasCuentas);
  var datosinsertmongo = JSON.parse(nuevasCuentas);
  //console.log(datosinsertmongo);
  //console.log(urlMlab + "?" + apiKey);
  clienteMlab = requestJson.createClient(urlMlab + "?" + apiKey);
  clienteMlab.post('',datosinsertmongo, function(err, resM, body){
    if(err)
    {
      console.log(resM);
      res.send("KO");
    }
    else
    {
      res.send("OK");
    }
  });
});

app.put('/addmovimiento', function(req, res){
  //console.log("Entro por PUT/addmovimiento");
  //console.log("Voy a insertar un nuevo movimiento en la cuenta " + req.body.idcuenta);
  //console.log("Primero recuepero el documento entero del cliente " + req.body.idusuario);
  //console.log(urlMlab
  //  + "?q={'idusuario':" + req.body.idusuario
  //  + "}&" + apiKey);
  clienteMlab = requestJson.createClient(urlMlab
    + "?q={'idusuario':" + req.body.idusuario
    + "}&" + apiKey);
  clienteMlab.get('', function(err, resM, body) {
    if(err)
    {
      console.log(JSON.stringify(resM));
    }
    else
    {
      //console.log(JSON.stringify(body));
      var documentoMongo = body;
      //console.log(documentoMongo);
      //console.log("Usuario documento Mongo " + documentoMongo[0].idusuario);

      //Nos situamos en la cuenta donde hay que insertar el movimiento
      for( var i = 0; i < documentoMongo[0].cuentas.length; i++ ) {
        if (documentoMongo[0].cuentas[i].idcuenta == req.body.idcuenta)
        {
          //Estamos situados en la cuenta donde tenemos que insertar el movimiento.
          //console.log("Estoy situado en la cuenta a insertar movimiento");

          //Modificamos el saldo de la cuenta
          var nuevosaldo = parseFloat(documentoMongo[0].cuentas[i].saldo);
          //console.log("El saldo antes de la cuenta es " + nuevosaldo);
          nuevosaldo = nuevosaldo + parseFloat(req.body.importe);
          //console.log("El saldo despues de la cuenta es " + nuevosaldo);

          documentoMongo[0].cuentas[i].saldo = nuevosaldo;

          //El indice del nuevo movimiento sera igual a la longitud del actual array
          //var j = documentoMongo[0].cuentas[i].movimientos.length;

          //Generamos el nuevo movimiento
          var fecha = "";
          fecha = req.body.fecha.substring(8,10) + "/"
                + req.body.fecha.substring(5,7) + "/"
                + req.body.fecha.substring(0,4);

          //console.log ("fecha movimiento " + fecha);

          var tipomovimiento = "";

          if (parseFloat(req.body.importe) < 0.00)
          {
            tipomovimiento = "Gasto";
          }
          else
          {
            tipomovimiento = "Ingreso";
          }

          var nuevomovimiento = {"fecha":fecha,
                                 "importe":parseFloat(req.body.importe),
                                 "tipo":tipomovimiento,
                                 "descripcion":req.body.detalle,
                                 "saldomovimiento":nuevosaldo};

          //console.log("nuevo movimiento " + nuevomovimiento);

          //Añadimos el nuevo movimiento mediante unshift
          documentoMongo[0].cuentas[i].movimientos.unshift(nuevomovimiento);

          //console.log(urlMlab
          //  + "?q={'idusuario':" + req.body.idusuario
          //  + "}&" + apiKey);
          //console.log(JSON.stringify(documentoMongo));
          clienteMlab = requestJson.createClient(urlMlab
            + "?q={'idusuario':" + req.body.idusuario
            + "}&" + apiKey);
          clienteMlab.put('',documentoMongo, function(err, resM, body){
            if(err)
            {
              console.log(resM);
              res.send("KO");
            }
            else
            {
              //console.log("Movimiento creado con exito");
              res.send("OK");
            }
          });
        }
      }
    }
  });
});

app.put('/addcuenta', function(req, res){
  //console.log("Entro por PUT/addcuenta");
  //console.log("Voy a insertar una nueva cuenta para el cliente  " + req.body.idusuario);
  //console.log(urlMlab
  //  + "?q={'idusuario':" + req.body.idusuario
  //  + "}&" + apiKey);
  clienteMlab = requestJson.createClient(urlMlab
    + "?q={'idusuario':" + req.body.idusuario
    + "}&" + apiKey);
  clienteMlab.get('', function(err, resM, body) {
    if(err)
    {
      console.log(JSON.stringify(resM));
    }
    else
    {
      //console.log(JSON.stringify(body));
      var documentoMongo = body;
      //console.log(documentoMongo);
      //console.log("Usuario documento Mongo " + documentoMongo[0].idusuario);

      //Recuperamos el idcuenta de la última cuenta del cliente
      var indiceultimacuenta = documentoMongo[0].cuentas.length - 1;
      var ultimacuenta = documentoMongo[0].cuentas[indiceultimacuenta].idcuenta;

      //console.log("La ultima cuenta es " + ultimacuenta);

      var patron04 = req.body.idusuario + "-";
      var numultimacuenta = ultimacuenta.replace(patron04, "");

      //console.log("Cuenta numero " + numultimacuenta);

      var numnuevacuenta = parseInt(numultimacuenta) + 1;

      //console.log("Numero de la nueva cuenta " + numnuevacuenta);

      var nuevacuenta = req.body.idusuario + "-" + numnuevacuenta;

      //console.log("La nueva cuenta es " + nuevacuenta);

      //Construimos el objeto de la nueva cuenta
      var fecha = new Date().toJSON().slice(0,10);

      //console.log(fecha);

      var fechaeditada = "";
      fechaeditada = fecha.substring(8,10) + "/"
                   + fecha.substring(5,7) + "/"
                   + fecha.substring(0,4);

      //console.log(fechaeditada);

      var nuevacuenta = {"idcuenta":nuevacuenta,
                         "saldo":0.00,
                         "divisa":"EUR",
                         "descripcion":req.body.detalle,
                         "movimientos":[
                                        {"fecha":fechaeditada,
                                         "importe":0.00,
                                         "tipo":"Apertura cuenta",
                                         "descripcion":"Apertura cuenta",
                                         "saldomovimiento":0.00
                                        }
                          ]
                        };

      //console.log(JSON.stringify(nuevacuenta));

      //Añadimos la nueva cuenta mediante push
      documentoMongo[0].cuentas.push(nuevacuenta);

      //console.log(urlMlab
      //  + "?q={'idusuario':" + req.body.idusuario
      //  + "}&" + apiKey);
      //console.log(JSON.stringify(documentoMongo));
      clienteMlab = requestJson.createClient(urlMlab
        + "?q={'idusuario':" + req.body.idusuario
        + "}&" + apiKey);
      clienteMlab.put('',documentoMongo, function(err, resM, body){
        if(err)
        {
          console.log(resM);
          res.send("KO");
        }
        else
        {
          //console.log("Cuenta creado con exito");
          //console.log(resM)
          res.send("OK");
        }
      });
    }
  });
});
